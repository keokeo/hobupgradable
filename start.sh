#!/bin/bash

echo "starting..."
if [ ! -d /main/app ]; then
  echo "No /main/app, downloading ${APP_URL}"
  curl -L -o /main/app.gz "${APP_URL}"
fi

echo "checking upgrade..."
if [ -f /main/app.gz ]; then
  echo "have new version, upgrading..."
  rm -rf /main/old
  mv /main/app /main/old
  cd /main && tar -zxf app.gz
  rm -rf /main/old
  rm /main/app.gz
fi

if [ -f /main/appsettings.json ]; then
  echo "copying appsettings..."
  cp appsettings.* main/app/
fi

echo "start /main/app/${MAIN_MODULE}"
dotnet /main/app/${MAIN_MODULE}
