FROM mcr.microsoft.com/dotnet/aspnet:6.0
RUN mkdir /main && apt-get update && apt-get install -y curl && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/
COPY ./start.sh /main/start.sh
RUN chmod +x /main/start.sh
WORKDIR /main
EXPOSE 80
ENTRYPOINT ["/main/start.sh"]
